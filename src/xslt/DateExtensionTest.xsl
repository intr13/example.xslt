<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="date">
    <xsl:output method="text"/>
    
    <xsl:template match="data">
        (<xsl:value-of select="date:year(birthday)*12+date:month-in-year(birthday)" />) 
        -
        (<xsl:value-of select="date:year(endTime)*12+date:month-in-year(endTime)" />)
        -
        (<xsl:value-of select="(date:year(endTime)*12+date:month-in-year(endTime))-(date:year(birthday)*12+date:month-in-year(birthday))" />)
    </xsl:template>
</xsl:stylesheet>