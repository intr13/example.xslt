<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        <xsl:variable name="exit">
            <xsl:call-template name="test"/>
        </xsl:variable>
        <xsl:if test="$exit = 'true'">
            <xsl:message terminate="yes">exit</xsl:message>
        </xsl:if>
        <xsl:for-each select="data/child">
            <xsl:call-template name="child"/>
        </xsl:for-each>        
    </xsl:template>
    
    <xsl:template name="test">
        <xsl:value-of select="1!=1"/>
    </xsl:template>
    
    <xsl:template name="child">   
        <xsl:value-of select="."/>
    </xsl:template>
</xsl:stylesheet>