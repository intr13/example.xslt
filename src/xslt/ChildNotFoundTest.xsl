<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        Children:
        <xsl:if test="not(data/child1)">
            Not found!
        </xsl:if>
        <xsl:apply-templates select="data/child1" 
                             mode="children"/>
        End.

        Children: 
        <xsl:if test="not(data/child2)">
            Not found!
        </xsl:if>
        <xsl:apply-templates select="data/child2" 
                             mode="children" />
        End.
         
        Children: 
        <xsl:if test="not(data/child1)">
            Not found!
        </xsl:if>
        <xsl:for-each select="data/child1">
            <xsl:call-template name="childProcessing"/>
        </xsl:for-each>
        End.

        Children: 
        <xsl:if test="not(data/child2)">
            Not found!
        </xsl:if>
        <xsl:for-each select="data/child2">
            <xsl:call-template name="childProcessing"/>
        </xsl:for-each>
        End.
    </xsl:template>
    
    <xsl:template match="child1"
                  mode="children">
        <xsl:value-of select="." /> - 1
    </xsl:template>
    
    <xsl:template name="childProcessing">
        <xsl:value-of select="." /> - 2
    </xsl:template>
</xsl:stylesheet>