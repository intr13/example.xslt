package xslt;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class MessageTest {
    public static void main(String[] args) {
        try {
            System.setProperty("javax.xml.transform.TransformerFactory",
                    "org.apache.xalan.processor.TransformerFactoryImpl");
            TransformerFactory factory = TransformerFactory.newInstance();
            Source xslt = new StreamSource(MessageTest.class.getResourceAsStream("MessageTest.xsl"));
            Transformer transformer = factory.newTransformer(xslt);
            Source input = new StreamSource(MessageTest.class.getResourceAsStream("MessageTest.xml"));
            Result output = new StreamResult(System.out);
            transformer.transform(input, output);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
