<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>

  <xsl:template match="/">
    <xsl:if test="/data/child">
      (is exists child) = true
    </xsl:if>
    <xsl:if test="not(/data/child)">
      (is not exists child) = false
    </xsl:if>
    <xsl:if test="/data/child1">
      (is exists child1) = false
    </xsl:if>
    <xsl:if test="not(/data/child1)">
      (is not exists child1) = true
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>