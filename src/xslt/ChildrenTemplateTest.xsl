<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        Children: 
        <xsl:apply-templates select="data/parent/child" 
                             mode="children" />
        End.
        
        Children: 
        <xsl:for-each select="data/parent/child">
            <xsl:call-template name="childProcessing"/>
        </xsl:for-each>
        End.
    </xsl:template>
    
    <xsl:template match="child"
                  mode="children">
        <xsl:value-of select="." /> - <xsl:value-of select="position()" /> - 1
    </xsl:template>
    
    <xsl:template name="childProcessing">
        <xsl:value-of select="." /> - <xsl:value-of select="position()" /> - 2
    </xsl:template>
</xsl:stylesheet>