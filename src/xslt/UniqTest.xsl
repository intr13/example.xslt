<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        <xsl:for-each select="data/child[not(id=preceding::id)]">
            <xsl:call-template name="child"></xsl:call-template>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="child">   
        <xsl:value-of select="name"/>
    </xsl:template>
</xsl:stylesheet>