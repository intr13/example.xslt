<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:string-utils="xalan://org.apache.commons.lang.StringUtils">
    <xsl:output method="text"/>
    
    <xsl:template match="data">
        <xsl:value-of select="string-utils:upperCase(string(.))" />
    </xsl:template>
</xsl:stylesheet>